# General  
- A reimplementation of Botesim in saner settings.  
- Built to learn and utilise R, with the Shiny and Leaflet libraries.  
- Graphics are fully new to this, after a minimalist NCURSES interface.  
- Currently turn-based; make choices as necessary and then press the big button to advance a chosen number of months.  
- The Lubridate package makes things just work, for the most part. No need to mess around with POSIXct.  
- Remember the `<<-` operator. It is extremely important and about as dangerous as a GOTO.  

# Map  
- Figures 1.15 and 1.16 in _Witsen_ provide essential groundwork.  
- Starting out with Leaflet, using the Stamen Watercolour tiles, since they're at least more appropriate than modern ones.    
- Where possible, for the purposes of Leaflet, locations of modern settlements can be used. Not ideal, but very easy to search.  
- As a first approximation, a map centred on Antwerp (lng 4.40346, lat 51.21989) at zoom 4 seems to do a good job of showing Europe.
- City co-ordinates taken from https://latitudelongitude.org/ .
- Might be able to do something with the NLmaps water layer at some point.  

# UI  
- Fundamentally mouse-driven, since that seems easiest with Shiny.  
- Map serves as a focal point, displaying everything.  
- Stock view has cities. Button toggles for areas of resource generation.  

# Save Files  
- In the interest of easy saving and loading as well as other handling, there is one universal chain of key-value pairs that serves as all data mutable within gameplay.  
- Things can be saved and loaded. There will, no doubt, be more that will be save-related and not fixed later.  
- Save is a long chain of key-value pairs.  
- Date is important. It's a core of the simulation.  
- Start new initialises things to 1600-01-01 with an to-be-tweaked fl. 100 000.  
- To track: date, currency and ships built.  
- TODO: get times to save and load properly. Might require more advanced JSON.  
- Save makes a note of all ships that are built in this timeline, as well as the commissions for them.    
- Save File Layout:  
 - Date  
 - Currency  
 - Save ID
 - List of Offers  
 - List of Undertaken Commissions  
 - List of Ships  
 - Lists can be their own objects, tied to a given save ID.  
- At the moment, it seems easier to have an ID associated with a save file, generated on new game, which is then associated with objects of that save so as to allow for multiple files and objects.  
- Save ID is chosen at a new game - the timestamp multiplied with a random 5-digit number appended.  

# Data structure  
- As noted when it comes to save, mutable data is all in one game state vector.   
- As before, GURPS Low-Tech Companion 3 provides some helpful simplifications for the purposes of modelling.  
- Core of transport is three adjacency matrices: land, river and sea, following the 25::5::1 rule of thumb for price per distance. This represents a departure from the original approach of linked lists.  
- The above is subject to change if I find some better way to handle things with Leaflet.  
- JSON is used to store data where applicable, since learning Jansson was part of the previous iteration.  
- WTB and WTS tables for lazy lookup, with the cities and a value between 0 (least relevant) and 1 (most relevant) for each good.  
- Currently only using 0 and 1, with a hacky solution to attract goods to a town of having sources want to buy and sell their good at 1.  
- For a simple model, all resources are funnelled towards shipbuilding towns.  
- In the future, this could be done with a real database.  
- At the moment, we don't consider hybrid river-sea travels. It goes from one port to another by one medium, regardless of shallow-draft ships designed for that sort of thing.  

## Locations  
- A location, such as a city, will have goods imported, goods exported and goods built, as well as linkages and, importantly, co-ordinates.  
### Key Resource Cities  
#### Wood  
- Dordrecht  
- Hamburg  
- Konigsberg  
- Riga  
- Norway (Find a Harbour)  
#### Iron and Coal  
- Spain, Sweden and Germany. Specifics follow.  
- Nuremberg steel  
- Gothenburg bars  
- Bergslandt (Hans Musiker) steel - does this exist?  
- Stockholm iron  
- Oregrund iron (high quality) - Note: required a different, online source of long/lat.  
- Danzig iron (better than Stockholm)
- Newcastle coal  
- Liege coal  
- Spain has Seville, Cádiz and Lisbon (yes, it's in Portugal, but it's still an important Iberian port) for iron.  
#### Hemp  
- Russia and Italy. Specifics if possible.  
- Best hemp is Italy (where?) then Riga, then Russia (where?).  
### General Resources  
- All fungible for now, despite Witsen making very clear notes on differing qualities.  
- Spawn within an area, to be bought up by the nearest trade city and shipped through the network by desires.  
- This allows us to get around Witsen's vagueness on certain sources, while being reasonably accurate in others - no need for random spawns to be in a particularly large area around Oregrund.  
-- TODO: get actual historical rates rather than having infinite supply bounded by transporation time and costs. Currently out of scope.  
-- TODO: More importantly: add intermediate goods. That way, resource cities can buy raw materials and sell intermediate goods, making everything make a lot more sense. Wood -> Logs, Iron -> Steel, etc.  

## Routes  
- For simplicity's sake, despite the obvious issues, land routes use modern distances, since they're easy to find. We can also abstract further to basic Haversine distances, but that's unnecessary if more accurate numbers can be found.    
- Prices: here we take some hacky figures from GURPS: Low-Tech. We use a simplified model of operating costs: we have our 1::5::25 rule of thumb for price per distance, then derive the price per distance over land and multiply.  
- We go with something simple. LTC3 offers an example of an ox cart for overland travel which serves as a general basis: 10 mi/day at 100lb of grain plus the price of the driver, given monthly and thus divided for a per-diem price.  
- We thus obtain figures: ignoring equipment, although realistically depreciation expenses would model things better, we take the farm price of 100lb of grain ($50) plus one thirtieth of the monthly $500 salary ($16.67) for the price per day.  
- At 10 mi/day, we thus obtain our base figure: on land, $66.67 per day for (for simplicity's sake) a third of a ton. This is far from perfect - it realistically varies significantly based on the distribution of feed and goods that can be carried and so on and represents a load of roughly half feed - but it gives us nice, hopefully reasonable, round numbers: we can generalise prices to $200 per tonne per day on land, or $20 per mile.  
- $20 per mile per ton on land thus, per the rule of thumb, means $4 per mile per ton by river and $0.8 per mile per ton by sea.  
- Continuing with very lazy equivalences, we take the short ton used here to be equal to the metric tonne. Given the imprecision of the assumption above, it makes very little difference.  
- One final step: converting from GURP$ to Dutch guilder. We note that the GURP$, as a 2004 USD, was about 1.3 2004 EUR, then reference this http://www.iisg.nl/hpw/data.php to obtain a value dated to the past. For convenience, we peg the conversion to the starting date of 1600: fl. 0.10 in 1600 = 1.3 EUR = $1 in 2004.  
- We thus obtain our final shipping rates per mile per tonne: fl. 2 on land, fl. 0.40 by river and fl. 0.08 by sea. Figures would be much messier going by kilometre, so despite the obvious advantages, here is where conversions stop.  

## Ships  
- Appendix A of the _Batavia_ book has a list of VOC ship charters. Very useful.  
- A bit of an afterthought to have the focus of the thing listed so far down, but that's the nature of the model.  
- Dates are all pretty arbitrary, with an assumption of a paradigm shift at 1650 - a convenient half-way point and also reasonably accurate - to larger ships and iron guns.  
- Labour is ignored for now, despite its great importance - we're still in the times before capital became the most important factor.  
- A ship is a data structure about its build requirements and eventual layout.  
- Said ship requires some(?) of each given material, although a substantial change arises in post-1650 with copper falling out of favour.  
- Demonstration ship is the pinas of Witsen's treatise, a mid-century medium-sized armed merchantman.  
- For earlier vessels, we look to the Batavia and the (decidedly-atypical) Vasa.  
- Rule of thumb ratio for dimensions: 4:1 length:beam and 10:1 length:depth.  
- Witsen helpfully provides a line-item breakdown of the cost of a 165-foot ship, which was apparently accurate enough that van Ijk (van Yk?) concurred. It's an overall price of fl. 93 635.  
- We have the tons burthen 
- No displacement right now, even though that might be a good measurement.  
- Ship object: 
 - Name  
 - Class  
 - Length  
 - Beam  
 - Sail Area (as a proxy for rig), in m^2  
 - Guns  
 - Capacity  
 - Date Laid Down  
 - Date Launched  
 - Value
- Use historical figures for length-beam ratio and capacity; the Batavia book has some good examples.  
- Classes are also historical, with both merchantmen and military ships. New classes open with time, especially with the establishment of the rating system in 1650.  
- Lengths can be given in metres, noting that historical figures might be given in all kinds of feet, most notably the 11" Amsterdam foot.  
- Per the _Batavia_ book, we treat 1 last as 2 tonnes.  
- The Batavia's charter says 600 tons, for what little that's worth.  
- When building a ship, we calculate material requirements, as well as a labour requirement in Guilders. It is presented in the shipbuilding interface as the costs.  
- Value comes from the commission that is fulfilled.  

### Ship Classes  
- Standardised, to be checked against.  
- Has properties:  
 - Class Name  
 - Purpose (Merchant/Military/Pleasure)  
 - First Available  
 - Min. Capacity  
 - Max. Capacity  
 - Class image (possibly, allowing for a display when viewing a ship of a given class).  
- Commissions pick a class that is desired and then roll normally within that class for their requirements.  
- For availability dates, we have the introduction of ratings around 1650, as well as the 1603, 1614, 1616 and 1653 VOC construction charters.  

# Commissions  
- How you make money.  
- Organisation puts out a contract for a certain type of ship, to be delivered at a certain time.  
- Untaken contracts have:
 - Sum  
 - Offerer  
 - Date Listed  
 - Date Expired  
 - Deadline (maybe?)  
 - Specifications (maybe just class for now?)  
- Undertaken contracts have:
 - Sum  
 - Offerer
 - Deadline (maybe?)  
 - Specifications (as above)  
- In their early state, commissions represent an ideal position to implement reactive variables with rigour.  

# Displaying Map Features  
- Add Poly Line  
